#!/usr/bin/python3
data = []
passport = ""
with open('input.txt', 'r') as f:
    for line in f:
        if line != "\n":
            passport += line
        else:
            data.append(passport)
            passport = ""

things = [
"byr:",
"iyr:", 
"eyr:", 
"hgt:",  
"ecl:", 
"pid:",
"hcl:"
]

s = 0

for passport in data:
    good = True

    for x in things:
        if x not in passport:
            good = False
            break

    if good:
        s += 1

print(s)