#!/usr/bin/python3
import re

data = []
passport = ""
with open('input.txt', 'r') as f:
    for line in f:
        if line != "\n":
            passport += line
        else:
            data.append(passport)
            passport = ""

easythings = [
re.compile("hcl:#[a-f0-9]{6}\s"),
re.compile("ecl:(amb|blu|brn|gry|grn|hzl|oth)\s"), 
re.compile("pid:\d{9}\s")
]

hardthings = [
re.compile("byr:(\d{4})\s"),
re.compile("iyr:(\d{4})\s"), 
re.compile("eyr:(\d{4})\s"), 
re.compile("hgt:(\d{2,3}[a-z]{2})\s"),
]

s = 0
print(len(data))

for passport in data:
    good = True

    for x in easythings:
        if x.search(passport) is None:
            good = False
            print(f"easyfail: {x.pattern}")
            break

    if not good:
        continue

    #verify years
    byr = hardthings[0].search(passport)
    iyr = hardthings[1].search(passport)
    eyr = hardthings[2].search(passport)

    if byr is None or iyr is None or eyr is None:
        print("No yr matches")
        continue

    byr = int(byr.group(1)) >= 1920 and int(byr.group(1)) <= 2002
    iyr = int(iyr.group(1)) >= 2010 and int(iyr.group(1)) <= 2020
    eyr = int(eyr.group(1)) >= 2020 and int(eyr.group(1)) <= 2030

    if not (byr and iyr and eyr):
        print("invalid yr")
        continue

    # height
    hgt = hardthings[3].search(passport)
    if hgt is None:
        print("No Hight Match")
        continue

    hgt = hgt.group(1)
    if hgt[-2:] == "cm":
        hgt = int(hgt[:-2])
        if not (hgt >= 150 and hgt <= 193):
            continue
    elif hgt[-2:] == "in":
        hgt = int(hgt[:-2])
        if not (hgt >= 59 and hgt <= 76):
            continue

    s += 1

print(s)