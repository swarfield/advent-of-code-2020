#!/usr/bin/python3
import functools

with open('input.txt', 'r') as f:
    data = [line for line in f]

trees = []
slopes = [(1,1),(3,1),(5,1),(7,1),(1,2)]

for x in slopes:
    col = 0
    num = 0
    for row in range(0,len(data), x[1]):
        if data[row][col] is '#':
            num += 1
        col += x[0]
        col = col % len(data[-1])
    trees.append(num)

print(functools.reduce(lambda x,y: x*y, trees))