#!/usr/bin/python3

data = []

with open('input.txt', 'r') as f:
    for line in f:
        data.append(line[:-1])

largest = 0

for addr in data:
    hob = addr[:-3]
    hob = hob.replace("B","1")
    hob = hob.replace("F","0")
    hob = int(hob,2)

    lob = addr[-3:]
    lob = lob.replace("R","1")
    lob = lob.replace("L","0")
    lob = int(lob, 2)

    addr = hob * 8 + lob
    if addr > largest:
        largest = addr

print(largest)