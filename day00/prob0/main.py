#!/usr/bin/python3

with open('input.txt', 'r') as f:
    nums = [int(x) for x in f.read().split()]

for x in range(len(nums)):
    for y in nums[x::]:
        if nums[x] + y == 2020:
            print(f"{nums[x]} {y}: {nums[x]*y}")
