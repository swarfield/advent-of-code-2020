#!/usr/bin/python3

with open('input.txt', 'r') as f:
    nums = [int(x) for x in f.read().split()]

for x in range(len(nums)):
    sub = nums[x::]
    for y in range(len(sub)):
        for z in sub[y::]:
            if nums[x] + sub[y] + z == 2020:
                print(f"{nums[x]} {sub[y]} {z}: {nums[x]*sub[y]*z}")
